package be.beeworks.streaming.tweet.mapper;

import be.beeworks.streaming.tweet.TweetDto;
import be.beeworks.streaming.tweet.model.Tweet;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
public class TweetMapper {
    public TweetDto getTweetDto(Tweet tweet) {
        if (tweet == null) throw new IllegalArgumentException("tweet cannot be null");
        TweetDto.Builder builder = TweetDto.newBuilder();
        builder.setId(tweet.getId());
        builder.setTimestamp(tweet.getTimestamp().getTime());
        builder.setHashtags(new ArrayList<>(tweet.getHashtags()));
        builder.setAuthor(tweet.getAuthor());
        builder.setText(tweet.getText());
        return builder.build();
    }

    public Tweet getTweet(TweetDto tweetDto) {
        if (tweetDto == null) throw new IllegalArgumentException("tweetDto cannot be null");
        return new Tweet()
                .withId(tweetDto.getId())
                .withTimestamp(new Date(tweetDto.getTimestamp()))
                .withHashtags(tweetDto.getHashtags())
                .withAuthor(tweetDto.getAuthor())
                .withText(tweetDto.getText());
    }
}
