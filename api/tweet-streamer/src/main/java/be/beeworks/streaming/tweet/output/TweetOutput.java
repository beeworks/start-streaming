package be.beeworks.streaming.tweet.output;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface TweetOutput {
    String TWEET_OUTPUT = "tweetOutput";

    @Output(TWEET_OUTPUT)
    MessageChannel tweetOutput();
}
