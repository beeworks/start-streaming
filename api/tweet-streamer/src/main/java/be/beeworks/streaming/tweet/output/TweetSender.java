package be.beeworks.streaming.tweet.output;

import be.beeworks.streaming.tweet.mapper.TweetMapper;
import be.beeworks.streaming.tweet.model.Tweet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

public class TweetSender {
    private static final Logger logger = LoggerFactory.getLogger(TweetSender.class);

    @Autowired @Qualifier(TweetOutput.TWEET_OUTPUT)
    private MessageChannel messageChannel;

    @Autowired
    private TweetMapper tweetMapper;

    public void send(Tweet tweet) {
        logger.info("sending tweet: {}", tweet);
        messageChannel.send(MessageBuilder.withPayload(tweetMapper.getTweetDto(tweet)).build());
    }
}
