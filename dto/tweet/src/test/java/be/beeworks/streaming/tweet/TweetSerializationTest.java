package be.beeworks.streaming.tweet;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileStream;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.junit.Assert.*;

public class TweetSerializationTest {
    private Random random = new Random();

    @Test
    public void testFullTweet() throws IOException {
        String text = UUID.randomUUID().toString();
        String author = UUID.randomUUID().toString();
        Long timestamp = random.nextLong();

        TweetDto resultingTweet = serializeAndDeserializeTweet(text, author, timestamp, Arrays.asList("spring", "cloud"));
        assertEquals(text, resultingTweet.getText());
        assertEquals(author, resultingTweet.getAuthor());
        assertEquals(timestamp, resultingTweet.getTimestamp());
        assertNotNull(resultingTweet.getId());
        assertNotNull(resultingTweet.getHashtags());
        assertEquals(2, resultingTweet.getHashtags().size());
        assertEquals("spring", resultingTweet.getHashtags().get(0));
        assertEquals("cloud", resultingTweet.getHashtags().get(1));
    }

    @Test
    public void testEmptyTweet() throws IOException {
        TweetDto resultingTweet = serializeAndDeserializeTweet(null, null, null, null);
        assertEquals("", resultingTweet.getText());
        assertEquals("", resultingTweet.getAuthor());
        assertNotNull(resultingTweet.getTimestamp());
        assertNull(resultingTweet.getHashtags());
    }

    private TweetDto serializeAndDeserializeTweet(String text, String author, Long timestamp, List<String> hashtags) throws IOException {
        TweetDto.Builder tweet1Builder = TweetDto.newBuilder().setId(UUID.randomUUID().toString());
        if (text != null) tweet1Builder.setText(text);
        if (author != null) tweet1Builder.setAuthor(author);
        tweet1Builder.setTimestamp(timestamp == null ? System.currentTimeMillis() : timestamp);
        if (hashtags != null) tweet1Builder.setHashtags(hashtags);
        TweetDto tweet1 = tweet1Builder.build();
        DatumWriter<TweetDto> tweetDatumWriter = new SpecificDatumWriter<>(TweetDto.class);
        DataFileWriter<TweetDto> dataFileWriter = new DataFileWriter<>(tweetDatumWriter);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        dataFileWriter.create(tweet1.getSchema(), outputStream);
        dataFileWriter.append(tweet1);
        dataFileWriter.close();

        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        DatumReader<TweetDto> tweetDatumReader = new SpecificDatumReader<>(TweetDto.class);
        DataFileStream<TweetDto> dataFileReader = new DataFileStream<>(inputStream, tweetDatumReader);

        assertTrue(dataFileReader.hasNext());
        return dataFileReader.next();
    }
}
