#!/bin/bash
# start a Docker container with JDK 8, copy the project to its working directory and link to the Kafka container,
# then build and run the application using gradle
docker run --rm -v "$PWD":/usr/src/app -w /usr/src/app/build/libs --link kafka:kafka -p 80:8080 openjdk:8 java -jar websocket-tweet-consumer.jar
