package be.beeworks.streaming.tweet;


import be.beeworks.streaming.tweet.input.TweetEventListener;
import be.beeworks.streaming.tweet.input.TweetInput;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBinding({TweetInput.class})
@ComponentScan("be.beeworks.streaming.tweet")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public TweetEventListener tweetEventListener() {
        return new TweetEventListener();
    }
}
