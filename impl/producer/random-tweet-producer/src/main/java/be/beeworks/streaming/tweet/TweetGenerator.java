package be.beeworks.streaming.tweet;

import be.beeworks.streaming.tweet.model.Tweet;
import be.beeworks.streaming.tweet.output.TweetSender;
import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class TweetGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private Lorem lorem = LoremIpsum.getInstance();

    @Autowired
    private TweetSender tweetWriter;

    @Autowired
    private AuthorGenerator authorGenerator;

    public void generateTweet() {
        Tweet tweet = new Tweet()
                .withAuthor(authorGenerator.randomAuthor())
                .withText(lorem.getWords(20))
                .withHashtags(Arrays.asList(lorem.getWords(1,5).split(" ")));
        logger.info("sending random tweet: {}", tweet);
        tweetWriter.send(tweet);
    }
}
